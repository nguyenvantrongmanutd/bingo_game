//generate an random order array
function randomArr() {
  var arr = [];
  for (var i = 1; i <= 99; i++) {
    arr.push(i);
  }
  arr.sort( function(a, b) {
    return 0.5 - Math.random()
  });

  return arr;
}

//genarate html cirle
function generateCirle(rowColVal, circleSize) {
  let arr = randomArr();
  let numberCircle = rowColVal*rowColVal;
  let htmlCircle = "";
  let heightCircle = (350 - (rowColVal + 1) * 10) / rowColVal; 
  for (var i = 1; i <= numberCircle; i++) {
    // check media type of window
    if (window.styleMedia.type == "print") {
      if (numberCircle % 2 != 0 && i == (parseInt(numberCircle / 2 ) + 1)) {
        htmlCircle += "<div class='circle' style='width: " + circleSize + "%; height: " + heightCircle + "px;'>";
        htmlCircle += "<p>&nbsp;</p></div>";
      } else {
        htmlCircle += "<div class='circle' style='width: " + circleSize + "%; height: " + heightCircle + "px;'>";
        htmlCircle += "<p style='line-height: " + (heightCircle+10) + "px'>" + arr[0] + "</p></div>";
        arr.shift();
      }

    } else if  (window.styleMedia.type == "screen") {
      if (numberCircle % 2 != 0 && i == (parseInt(numberCircle / 2 ) + 1)) {
        htmlCircle += "<div class='circle' style='width: " + circleSize + "%; height: " + circleSize + "%;'>";
        htmlCircle += "<p>&nbsp;</p></div>";
      } else {
        htmlCircle += "<div class='circle' style='width: " + circleSize + "%; height: " + circleSize + "%;'>";
        htmlCircle += "<p style='line-height: " + heightCircle + "px'>" + arr[0] + "</p></div>";
        arr.shift();
      }
    }
  }

  return htmlCircle;
}

//event click button
function submit() {
  let ticketVal = document.getElementById("ticket").value;
  let rowColVal = parseInt(document.getElementById("row-col").value);

  //validate
  if (ticketVal == "" || rowColVal == "") {
    alert("Hãy nhập giá trị!");
  
  //validate
  } else if (rowColVal != 5 && rowColVal != 6 && rowColVal != 7) {
    alert("Sai giá trị!");
  
  //valid
  } else {
    // let circleSize = (100 - (rowColVal + 1) * 3) / rowColVal;
    let circleSize = (100 - (rowColVal + 1) * 3) / rowColVal;
    let ele = document.getElementById("menu-screen");
    ele.parentNode.removeChild(ele);
    let containerEle = document.getElementById("container");
    let html = "";
    for (let i = 1; i <= ticketVal; i++) {
      if (i % 2 == 0) {
        html += "<div class='ticket-right'><div class='container-circle'>"+generateCirle(rowColVal, circleSize)+"</div></div>";
        html += "</div>";
      } else {
        html += "<div class='ticket-couple'>";
        html += "<div class='ticket-left'><div class='container-circle'>"+generateCirle(rowColVal, circleSize)+"</div></div>";
      }
    }
    containerEle.innerHTML = html;
  } 
}